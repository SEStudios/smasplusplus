local level_dependencies_normal= require("level_dependencies_normal")

function onLoadSection0()
    smasnoturnback.enabled = false
end

function onLoadSection1()
    smasnoturnback.enabled = true
end

function onLoadSection2()
    smasnoturnback.enabled = true
end

function onLoadSection3()
    smasnoturnback.enabled = false
end